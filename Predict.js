const tf = require('@tensorflow/tfjs-node');
const fs = require('fs');
const PNG = require("pngjs").PNG;


const input = [];
let path = 'Inputs/Two.png';

if (process.argv.length > 2) {
    path = process.argv[2];
}

loadModel();
async function loadModel() {
    const model = await tf.loadLayersModel('file://my-model/model.json');

    fs.createReadStream(path)
        .pipe(
            new PNG({
                filterType: 4,
            })
        )
        .on("parsed", function () {
            for (var y = 0; y < this.height; y++) {
                for (var x = 0; x < this.width; x++) {
                    var idx = (this.width * y + x) << 2;

                    this.data[idx] = this.data[idx];
                    this.data[idx + 1] = this.data[idx + 1];
                    this.data[idx + 2] = this.data[idx + 2];

                    input.push((this.data[idx] + this.data[idx + 1] + this.data[idx + 2]) / 3);

                }
            }
            predict(model,input);
        });

}


function predict(model,inputData) {

    tf.tidy(() => {
        inputData = tf.tensor([inputData]);

        let output = model.predict(inputData).dataSync();
        let maxVal = Math.max(...output);
        let digit = output.indexOf(maxVal);

        console.log('DIGIT: ', digit);
    })
}




