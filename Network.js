const tf = require('@tensorflow/tfjs-node-gpu');
const fs = require('fs');

const dataFileBuffer  = fs.readFileSync(__dirname + '/Datasets/train-images.idx3-ubyte');
const labelFileBuffer  = fs.readFileSync(__dirname + '/Datasets/train-labels.idx1-ubyte');
const pixelValues = [];
const labelValues = [];

const _HIDDEN_NODES = 128;
const _INPUT_NODES = 784;
const _ACTIVATION = 'sigmoid';
const _OUTPUT_NODES = 10;
const model = tf.sequential();

function decodePixelsData(dataFileBuffer,labelFileBuffer)
{

    for (var image = 0; image <= 59999; image++) { 
        var pixels = [];
        
        for (var y = 0; y <= 27; y++) {
            for (var x = 0; x <= 27; x++) {
                pixels.push(dataFileBuffer[(image * 28 * 28) + (x + (y * 28)) + 16]);
            }
        }
        pixelValues.push(pixels);
        let temp = new Array(10).fill(0);
        temp[+JSON.stringify(labelFileBuffer[image + 8])] = 1;
        labelValues.push(temp);
    }
}

function createModel() 
{
    const input = tf.layers.dense(({
        units: _HIDDEN_NODES,
        inputShape: [_INPUT_NODES],
        activation: _ACTIVATION
    }));

    const hidden = tf.layers.dense({
        units: _HIDDEN_NODES,
        activation: _ACTIVATION
    })
    const hidden1 = tf.layers.dense({
        units: _HIDDEN_NODES,
        activation: _ACTIVATION
    })
    const hidden2 = tf.layers.dense({
        units: _HIDDEN_NODES,
        activation: _ACTIVATION
    })
    const hidden3 = tf.layers.dense({
        units: _HIDDEN_NODES,
        activation: _ACTIVATION
    })

    const output = tf.layers.dense({
        units: _OUTPUT_NODES,
        activation: 'softmax'
    })

    model.add(input);
    model.add(hidden);
    model.add(hidden1);
    model.add(hidden2);
    model.add(hidden3);
    model.add(output);
}

function predict(inputData,predictedOutput=0)
{
    tf.tidy(()=>{
        inputData = tf.tensor([inputData]);

        let output = model.predict(inputData).dataSync();
        let maxVal = Math.max(...output);
        let digit = output.indexOf(maxVal);
    })
}

let test = new Array(784).fill().map(a=>Math.random());

createModel();
predict(test);
decodePixelsData(dataFileBuffer,labelFileBuffer);

function onBatchEnd(batch, logs)
{
    console.log('Accuracy', logs.acc);
}

model.compile({
    optimizer: 'adam',
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy']
  });

async function save()
{
    await model.save('file:///NIU/my-model');
}

model.fit(tf.tensor(pixelValues),tf.tensor(labelValues),{
    epochs: 200,
    batchSize: 512,
    callbacks: {onBatchEnd}
}).then(info=>{
    console.log('Final accuracy', info.history.acc);
    save();
});




