"use strict";
var canvas = document.getElementById('sheet');
var context = canvas.getContext("2d");

let hCanvas = document.getElementById('hiddenCanvas');
let hContext = hCanvas.getContext("2d");


context.strokeStyle = 'black';
context.fillRect(0, 0, context.canvas.width, context.canvas.height);
context.strokeStyle = "white";
context.lineJoin = "round";
context.lineWidth = 40;

var clickX = [];
var clickY = [];
var clickDrag = [];
var paint;
let contentURL;

/**
 * Add information where the user clicked at.
 * @param {number} x
 * @param {number} y
 * @return {boolean} dragging
 */
function addClick(x, y, dragging) {
    clickX.push(x);
    clickY.push(y);
    clickDrag.push(dragging);
}

/**
 * Draw the newly added point.
 * @return {void}
 */
function drawNew() {
    var i = clickX.length - 1
    if (!clickDrag[i]) {
        if (clickX.length == 0) {
            context.beginPath();
            context.moveTo(clickX[i], clickY[i]);
            context.stroke();
        } else {
            context.closePath();

            context.beginPath();
            context.moveTo(clickX[i], clickY[i]);
            context.stroke();
        }
    } else {
        context.lineTo(clickX[i], clickY[i]);
        context.stroke();
    }
}

function mouseDownEventHandler(e) {
    paint = true;
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    if (paint) {
        addClick(x, y, false);
        drawNew();
    }
}

function touchstartEventHandler(e) {
    paint = true;
    if (paint) {
        addClick(e.touches[0].pageX - canvas.offsetLeft, e.touches[0].pageY - canvas.offsetTop, false);
        drawNew();
    }
}

function mouseUpEventHandler(e) {
    context.closePath();
    paint = false;
}

function mouseMoveEventHandler(e) {
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    if (paint) {
        addClick(x, y, true);
        drawNew();
    }
}

function touchMoveEventHandler(e) {
    if (paint) {
        addClick(e.touches[0].pageX - canvas.offsetLeft, e.touches[0].pageY - canvas.offsetTop, true);
        drawNew();
    }
}

function setUpHandler(isMouseandNotTouch, detectEvent) {
    removeRaceHandlers();
    if (isMouseandNotTouch) {
        canvas.addEventListener('mouseup', mouseUpEventHandler);
        canvas.addEventListener('mousemove', mouseMoveEventHandler);
        canvas.addEventListener('mousedown', mouseDownEventHandler);
        mouseDownEventHandler(detectEvent);
    } else {
        canvas.addEventListener('touchstart', touchstartEventHandler);
        canvas.addEventListener('touchmove', touchMoveEventHandler);
        canvas.addEventListener('touchend', mouseUpEventHandler);
        touchstartEventHandler(detectEvent);
    }
}

function mouseWins(e) {
    setUpHandler(true, e);
}

function touchWins(e) {
    setUpHandler(false, e);
}

function removeRaceHandlers() {
    canvas.removeEventListener('mousedown', mouseWins);
    canvas.removeEventListener('touchstart', touchWins);
}

function Clear()
{
    context.strokeStyle = 'black';
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);
    context.strokeStyle = 'white';
}

function RecognizeNumber()
{
    let pixels = [];
    hContext.drawImage(canvas,4,4,0.05*canvas.width,0.05*canvas.height);
    let image = hContext.getImageData(0,0,28,28);
    image.data.map((x,i)=>i%4?0:pixels.push(x));

    loadModel(pixels);
}

async function loadModel(input) {
    const model = await tf.loadLayersModel('https://bartoszbielinski.ddns.net/pawlacz/model.json');
    predict(model,input);
}


function predict(model,inputData) {

    tf.tidy(() => {
        inputData = tf.tensor([inputData]);

        let output = model.predict(inputData).dataSync();
        let maxVal = Math.max(...output);
        let digit = output.indexOf(maxVal);

        document.getElementById('Result').innerHTML = 'Your digit is: ' + digit;
        
        console.log('DIGIT: ', digit);
    })
}

document.addEventListener('copy', (event) => {
    event.clipboardData.setData('text/plain', contentURL);
    event.preventDefault();
});

canvas.addEventListener('mousedown', mouseWins);
canvas.addEventListener('touchstart', touchWins);